<?php

//   ---------------------------------------
//    基本設定
//  ----------------------------------------

//管理画面 [設定 - メディア] で指定する 「大サイズ」 の幅の上限
	if ( ! isset( $content_width ) )
		$content_width = 720;

	//サムネイルサイズ
		add_theme_support('post-thumbnails');
		add_image_size( 'original_thumb', 300, 300, false );
		add_image_size( 'original_thumbsq', 300, 300, true );
		add_image_size( 'original_thumb_lrg', 500, 500, false );
		add_image_size( 'original_thumbsq_lrg', 500, 500, true );
		add_image_size( 'original_thumb_landscape', 400, 225, true );
		add_image_size( 'original_thumb_portrait', 225, 400, true );
		add_image_size( 'original_portrait', 450, 800, true );
		add_image_size( 'original_landscape', 800, 450, true );
		add_image_size( 'original_landscape_lrg', 1200, 600, true );


		//サムネイルサイズ
		add_theme_support( 'html5', array('search-form', 'comment-form', 'comment-list',) );
		add_theme_support( 'post-formats', array('aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery',) );

?>
