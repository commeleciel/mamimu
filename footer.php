
<footer>
    <div class="footer_wrap">
        
        <p>sitemap</p>
        
        <div class="flex">
            <ul class="footer_nav">
                <li>
                    <ul>
                        <li><a href="#">HOME</a></li>
                        <li><a href="#">私たちについて</a></li>
                        <li><a href="#"><span></span>団体概要</a></li>
                        <li><a href="#"><span></span>活動実績</a></li>
                        <li><a href="#"><span></span>メディア掲載</a></li>
                    </ul>
                </li>
                <li>
                    <ul>
                        <li><a href="#">活動内容</a></li>
                        <li><a href="#"><span></span>チームワーク</a></li>
                        <li><a href="#"><span></span>ナレッジシェア</a></li>
                        <li><a href="#"><span></span>プロジェクト</a></li>
                    </ul>
                </li>
                <li>
                    <ul>
                        <li><a href="#">メンバー紹介</a></li>
                        <li><a href="#"><span></span>ウェブ</a></li>
                        <li><a href="#"><span></span>グラフィック</a></li>
                        <li><a href="#"><span></span>ライター</a></li>
                        <li><a href="#"><span></span>カメラ</a></li>
                        <li><a href="#"><span></span>マルチ</a></li>
                        <li><a href="#"><span></span>その他専門</a></li>
                        <li><a href="#"><span></span>メンバー制について</a></li>
                    </ul>
                </li>
                <li>
                    <ul>
                        <li><a href="#">ポートフォリオ</a></li>
                        <li><a href="#">お問い合わせ</a></li>
                        <li><a href="#">メールマガジン</a></li>
                        <li><a href="#">Facebook</a></li>
                    </ul>
                </li>
            </ul>
            
            <div class="footer_fb">
                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fsuginamimamimu%2F&tabs&width=340&height=214&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="340" height="214" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
            </div>
            
        </div>
        
        <div>
            <div class="share_tw">
                
            </div>
            <div class="share_fb">
                
            </div>
            <ul>
                <li>
                    <a href="#">プライバシーポリシー</a>
                </li>
                <li>
                    <a href="#">サイトマップ</a>
                </li>
            </ul>
        </div>
        
    </div>
</footer>

</div>
</body>
</html>