<?php

require_once locate_template('modules/fun_init.php');						//	WordPressの初期設定(サムネイルとか)
require_once locate_template('modules/fun_java-css.php');				//	CSS & Javascriptの読み込み
require_once locate_template('modules/fun_output-ext.php');				//	テキストなどの出力回り
require_once locate_template('modules/fun_getpost.php');				//	pre_get_post
require_once locate_template('modules/fun_admin.php');				//	管理画面関係
require_once locate_template('modules/fun_plugins.php');				//	プラグイン関係
require_once locate_template('modules/fun_other.php');				//	その他の設定

?>
