<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=0.5,user-scalable=yes,initial-scale=1.0">
<title>mamimu | 育児中の働くを考える</title>

<!-- CSS -->
<link rel="stylesheet" media="all" href="assets/css/default.css">
<link rel="stylesheet" media="all" href="assets/ccss/font-awesome.min.css">
<link rel="stylesheet" media="all" href="assets/ccss/style.css" />
<!-- //CSS -->

<!-- JS -->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/js/jquery-2.2.4.min.js"><\/script>')</script>
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
<script type="text/javascript" src="assets/js/config.js"></script>
<!-- //JS -->

<body>
<div class="bodywrap">
    
<header>
    <div class="header_wrap">
        <div>
            <h1><img src="" alt="mamimu マミム"></h1>
        </div>
        <nav class="pcnav">
            <ul>
                <li><a href="#">私たちについて</a>
                    <ul>
                        <li><a href="#">団体概要</a></li>
                        <li><a href="#">活動履歴</a></li>
                        <li><a href="#">メディア掲載</a></li>
                    </ul>
                </li>
                <li><a href="#">お知らせ</a></li>
                <li><a href="#">活動内容</a>
                    <ul>
                        <li><a href="#">チームワーク</a></li>
                        <li><a href="#">ナレッジシェア</a></li>
                        <li><a href="#">プロジェクト</a></li>
                    </ul>
                </li>
                <li><a href="#">メンバー</a>
                    <ul>
                        <li><a href="#">ウェブ</a></li>
                        <li><a href="#">グラフィック</a></li>
                        <li><a href="#">ライター</a></li>
                        <li><a href="#">カメラマン</a></li>
                        <li><a href="#">マルチ</a></li>
                        <li><a href="#">その他専門</a></li>
                    </ul>
                </li>
                <li><a href="#">お問い合わせ</a></li>
            </ul>
        </nav>
    </div>
</header>